﻿using System;
using TeleConsole.Client;
using System.Threading;

namespace ClientTest
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			TeleConsoleClient remoteConsole = new TeleConsoleClient();
			remoteConsole.MirrorToLocalConsole = true;
			remoteConsole.Connect();
			while(true)
			{
				remoteConsole.WriteLine("Testing.");
				Thread.Sleep(1000);
			}
		}
	}
}
