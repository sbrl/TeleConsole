# TeleConsole

> A simple remote console to aid debugging of programs in awkward situations.

## Install

### Client
The client can be installed in multiple ways. There's a [nuget package](https://www.nuget.org/packages/TeleConsoleClient/) you can install from the MonoDevelop or Visual Studio interface. Here's how to do that from the command-line though:

```bash
nuget install TeleConsoleClient
```

The code from [TeleConsoleClient.cs](https://gitlab.com/sbrl/TeleConsole/blob/master/TeleConsole-Client/TeleConsoleClient.cs) can also be copied directly to your project, but beware that you'll need to check back from time to time for updates.

### Server
The server is just a simple binary that listens on a TCP port and echoes everything from the connections it gets. A CIL binary is provided on the page of the latest release on the [releases page](https://gitlab.com/sbrl/TeleConsole/tags/). Don't forget to click on the latest tag name to get more information on each release! GitLab doesn't appear to show anything more than the first part of the first line of the release notes otherwise.....

## Usage
The TeleConsole client is designed to feel a lot like a combination of the `TcpClient` and `Console` classes. Here's a simple example:

```csharp
TeleConsoleClient remoteConsole = new TeleConsoleClient();
remoteConsole.WriteLine("Hello, {0}!", "galaxy");
```

The above connects to localhost on port 4021, by default. This can be changed like so:

```csharp
TeleConsoleClient remoteConsole = new TeleConsoleClient(IPAddress.Parse("1.2.3.4"), 8888);
remoteConsole.WriteLine(
	"Hello, {0}! We can have as many parameters as we like: {1} {2} {3} {4} {5} {6} {7}",
	"galaxy",
	"yay", IPAddress.IPv6Loopback, 67, 42, "cheese", 'd', new Rocket()
);
```

The above connects to port 8888 on 1.2.3.4. More advanced usage can be found by [reading the code](https://gitlab.com/sbrl/TeleConsole/blob/master/TeleConsole-Client/TeleConsoleClient.cs) - don't worry, it's fairly short and well documented :D If it does become too long and / or cumbersome to read, I'll expand this section :-)

### Server Commands
While you can type in anything you like into the server prompt, a few characters have a special meaning. These are documented below:

Character	| Meaning
------------|---------------
`-`			| Draws a horizontal rule
`=`			| Draws a bunch of new-line characters

## Contributing
(Detailed) bug reports and suggestions are welcome! Just [open an issue](https://gitlab.com/sbrl/TeleConsole/issues/new) :smiley_cat:

Pull Requests are also welcome :smile_cat:

## License
TeleConsole is licensed under the _Mozilla Public License 2.0_, a copy of which (with a summary) can be found [here](https://gitlab.com/sbrl/TeleConsole/blob/master/LICENSE).
