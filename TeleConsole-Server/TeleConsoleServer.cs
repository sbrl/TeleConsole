﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace TeleConsole.Server
{
	public class TeleConsoleServer
	{
		public bool Quiet = false;
		public IPAddress BindAddress { get; private set; }
		public int Port { get; private set; }
		public int NextId { get; private set; }

		TcpListener listener;

		Random rand = new Random();

		public TeleConsoleServer(IPAddress inBindAddress, int inPort)
		{
			BindAddress = inBindAddress;
			Port = inPort;
		}

		/// <summary>
		/// Starts the TeleConsole server listening for connections.
		/// </summary>
		public async Task Start()
		{
			listener = new TcpListener(BindAddress, Port);
			listener.Start();

			WriteLine("Started listening for connections on {0}:{1}.", Utilities.FormatIpAddress(BindAddress), Port);

			while (true)
			{
				TcpClient nextClient = await listener.AcceptTcpClientAsync();
				WriteLine("\u001b[32m\u001b[1mNew connection from {0}.\u001b[0m", nextClient.Client.RemoteEndPoint);
				Utilities.ForgetTask(HandleConnection(nextClient));
			}
		}

		public async Task HandleConnection(TcpClient connection)
		{
			int connectionId = NextId++;
			// Make each connection a random colour
			string remoteEndpointText = Utilities.FormatIpEndpoint(connection.Client.RemoteEndPoint);
			string connectionColour = $"\u001b[{rand.Next(31, 37)}m";
			StreamReader incoming = new StreamReader(connection.GetStream());
			StreamWriter outgoing = new StreamWriter(connection.GetStream());
			string nextLine = string.Empty;
			while(true)
			{
				// Make sure that we are actually connected - sometimes clients crash and get forcefully 
				// Also make sure that we catch all the data that's been sent to us - there may be extra data sent that 
				// we haven't read yet, even though the remote host has been disconnected.
				if(!connection.Connected && connection.Available == 0)
					break;

				try
				{
					nextLine = await incoming.ReadLineAsync();
				}
				catch(Exception error)
				{
					WriteLine("\u001b[31m\u001b[1m[#{0} - {1}] Read error: {2}\u001b[0m", connectionId, remoteEndpointText, error.Message);
					continue;
				}
				if (nextLine == null)
					break;

				WriteLine("{0}[#{1} - {2}]\u001b[0m {3}", connectionColour, connectionId, remoteEndpointText, nextLine);
			}

			WriteLine("\u001b[31m\u001b[1mLost connection from {0}.\u001b[0m", remoteEndpointText);
			connection.Close();
			incoming.Close();
			outgoing.Close();
		}

		public void WriteLine(string str, params object[] insertionObjects)
		{
			if(Quiet) return;

			Console.WriteLine("\u001b[33m\u001b[1m[" + DateTime.Now.ToLongTimeString() + "]\u001b[0m " + str, insertionObjects);
		}
	}
}

