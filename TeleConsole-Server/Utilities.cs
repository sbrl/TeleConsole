﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Net;
using System.Reflection;

namespace TeleConsole
{
	public class Utilities
	{
		/// <summary>
		/// Call this method to allow a given task to complete in the background.
		/// Errors will be handled correctly.
		/// Useful in fire-and-forget scenarios, like a TCP server for example.
		/// From http://stackoverflow.com/a/22864616/1460422
		/// </summary>
		/// <param name="task">The task to forget about.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions. Exceptions specified here won't cause a crash.</param>
		public static async void ForgetTask(Task task, params Type[] acceptableExceptions)
		{
			try
			{
				await task.ConfigureAwait(false);
			}
			catch (Exception ex)
			{
				// TODO: consider whether derived types are also acceptable.
				if (!acceptableExceptions.Contains(ex.GetType()))
					throw;
			}
		}

		/// <summary>
		/// Formats an ip address for display, surrounding ipv6 addresses in square brackets.
		/// </summary>
		/// <returns>The formatted ip address.</returns>
		/// <param name="ip">The ip address to format.</param>
		public static string FormatIpAddress(IPAddress ip)
		{
			string ipText = ip.ToString();
			if (ipText.Contains(":"))
				return string.Format("[{0}]", ipText);
			else
				return ip.ToString();
		}

		public static string FormatIpEndpoint(IPEndPoint ipe)
		{
			return string.Format("{0}:{1}", FormatIpAddress(ipe.Address), ipe.Port);
		}
		public static string FormatIpEndpoint(EndPoint ipe)
		{
			return FormatIpEndpoint((IPEndPoint)ipe);
		}

		/// <summary>
		/// Gets the date and time that the binary was built.
		/// </summary>
		/// <remarks>
		/// From SpritePacker's Utility.cs: https://git.starbeamrainbowlabs.com/sbrl/SpritePacker/src/master/SpritePacker/Utilities.cs
		/// Originally from stack overflow somewhere.
		/// </remarks>
		/// <returns>A DateTime object representing the date and time the top level program was built.</returns>
		public static DateTime GetProgramBuildDate()
		{
			Version asmVersion = GetProgramVersion();
			return new DateTime(2000, 1, 1).Add(new TimeSpan(
				TimeSpan.TicksPerDay * asmVersion.Build +
				TimeSpan.TicksPerSecond * asmVersion.Revision * 2
			));
		}
		/// <summary>
		/// Gets the version of the top-level executing program.
		/// </summary>
		/// <returns>The version of the top-level executing program</returns>
		public static Version GetProgramVersion()
		{
			return Assembly.GetEntryAssembly().GetName().Version;
		}
	}
}

