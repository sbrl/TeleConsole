﻿using System;
using System.Net;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace TeleConsole.Server
{
	class MainClass
	{
		public static int Main(string[] args)
		{
			bool quiet = false;
			List<string> extras = new List<string>();

			IPAddress bindAddress = IPAddress.IPv6Loopback;
			int port = 4021;

			for(int i = 0; i < args.Length; i++)
			{
				if(!args[i].StartsWith("-"))
				{
					extras.Add(args[i]);
					continue;
				}
				bool isEndParam = i + 1 >= args.Length;
				switch(args[i].Trim('-'))
				{
					case "version": // Display version information
						Console.Error.WriteLine("TeleConsoleServer");
						Console.Error.WriteLine("-----------------");
						Console.Error.WriteLine("Version {0}", Utilities.GetProgramVersion());
						Console.Error.WriteLine("Build timestamp: {0}", Utilities.GetProgramBuildDate());
						return 0;
					case "help": // Display help text
						Assembly asm = Assembly.GetExecutingAssembly();
						/*** Debug - Lists the names of all embedded resources ***
						foreach(string str in asm.GetManifestResourceNames())
							Console.WriteLine(str);*/
						StreamReader helpTextReader = new StreamReader(asm.GetManifestResourceStream(@"TeleConsole.Server.Help.txt"));
						string helpText = helpTextReader.ReadToEnd();
						helpTextReader.Dispose();
						Console.Error.WriteLine(helpText);
						return 0;
					case "h": // bind address
						if(isEndParam) continue;
						bindAddress = IPAddress.Parse(args[++i]);
						break;
					case "p": // port
						if(isEndParam) continue;
						port = int.Parse(args[++i]);
						break;
					case "q":
						quiet = true;
						break;
					default:
						Console.Error.WriteLine("Error: Unknown flag '{0}'.", args[i]);
						return 1;
				}
			}

			if(!quiet) {
				string header = $"TeleConsole Server v{Utilities.GetProgramVersion()}, built on {Utilities.GetProgramBuildDate()}";

				Console.WriteLine(header);
				Console.WriteLine(new string('-', header.Length));
			}

			TeleConsoleServer server = new TeleConsoleServer(bindAddress, port) { Quiet = quiet };
			Task[] serverTasks = new Task[] {
				server.Start(),
				DiscardStandardInput()
			};

			Task.WaitAll(serverTasks);
			return 0;
		}

		public static async Task DiscardStandardInput()
		{
			StreamReader stdin = new StreamReader(Console.OpenStandardInput());
			while (true) {
				string nextLine = await stdin.ReadLineAsync();
				switch (nextLine.Trim()[0])
				{
					case '-':
						Console.WriteLine("\u001b[1A      \n\n\n" + new string('-', 79) + "\n");
						break;
					case '=':
						Console.WriteLine("\u001b[1A      " + new string('\n', 25));
						break;
				}
			}
		}
	}
}
