using System;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace TeleConsole.Client
{
    /// <summary>
    /// The TeleConsole Client class. Use to connect and log to a remote console server.
    /// </summary>
    /// <version>v0.3</version>
    /// <changelog>
    /// v0.2: 3rd November 2017
    ///      - Added this changelog
    ///      - Added invisible mode
    /// </changelog>
    public class TeleConsoleClient : IDisposable
    {
        /// <summary>
        /// The default remote address to connect to.
        /// </summary>
        public static readonly IPAddress DefaultRemoteAddress = IPAddress.IPv6Loopback;
        /// <summary>
        /// The default port to connect to.
        /// </summary>
        public static readonly int DefaultPort = 4021;

        private bool mirrorToLocalConsole = false;

        /// <summary>
        /// The remote address that the console client will connect or is connected to.
        /// </summary>
        public IPAddress RemoteAddress { get; private set; }
        /// <summary>
        /// The port that the console client will connect or is connected to.
        /// </summary>
        public int Port { get; private set; }
        /// <summary>
        /// Whether to send the result of all Write() and WriteLine() calls to the local console as well as the remote
        /// one.
        /// </summary>
        public bool MirrorToLocalConsole
        {
            get { return mirrorToLocalConsole; }
            set { mirrorToLocalConsole = value; }
        }

        /// <summary>
        /// Invisible mode ensures that TeleConsole is silent when the server can't be contacted. 
        /// </summary>
        public bool InvisibleMode { get; private set; } = true;

        /// <summary>
        /// The underlying connection.
        /// </summary>
        TcpClient client;
        /// <summary>
        /// The outgoing stream writer.
        /// </summary>
        StreamWriter outgoing;
        /// <summary>
        /// The incoming stream reader.
        /// </summary>
        StreamReader incoming;

        /// <summary>
        /// Initializes a new instance of the TeleConsoleClient class with all  the default settings.
        /// </summary>
        public TeleConsoleClient() : this(DefaultRemoteAddress) { }
        /// <summary>
        /// Initializes a new instance of the TeleConsoleClient class with the specified remote console server address
        /// and the default port.
        /// </summary>
        /// <param name="inRemoteAddress">The remote address of the console server.</param>
        public TeleConsoleClient(IPAddress inRemoteAddress) : this(inRemoteAddress, DefaultPort) { }
        /// <summary>
        /// Initializes a new instance of the TeleConsoleClient class with the specified remoe console server address
        /// and port number.
        /// </summary>
        /// <param name="inRemoteAddress">The remote address of the console server.</param>
        /// <param name="inPort">The port that the remote console server is running on.</param>
        public TeleConsoleClient(IPAddress inRemoteAddress, int inPort)
        {
            RemoteAddress = inRemoteAddress;
            Port = inPort;
        }

        /// <summary>
        /// Connects to the remote console.
        /// </summary>
        public void Connect()
        {
            try
            {
                client = new TcpClient(RemoteAddress.ToString(), Port);
                incoming = new StreamReader(client.GetStream());
                outgoing = new StreamWriter(client.GetStream())
                {
                    // Automatically flush the outgoing stream as often as possible
                    AutoFlush = true
                };
            }
            catch
            {
                if (!InvisibleMode)
                    throw;
            }
        }

        /// <summary>
        /// Writes a string to the remote console.
        /// </summary>
        /// <param name="format">The string to write.</param>
        /// <param name="insertionObjects">Optional objects to pass to string.Format.</param>
        public void Write(string format, params object[] insertionObjects)
        {
            if (outgoing == null && !InvisibleMode)
                throw new InvalidOperationException("The client doesn't appear to be connected, so you can't write " +
                    "to it. Perhaps you forgot to call Connect()?");

            string logString = string.Format(format, insertionObjects);

            try
            {
                outgoing.Write(logString);
            }
            catch
            {
                if (!InvisibleMode)
                    throw;
            }
            if (MirrorToLocalConsole)
                Console.Write(logString);
        }
        /// <summary>
        /// Writes a line to the remote console.
        /// </summary>
        /// <param name="format">The string to write.</param>
        /// <param name="insertionObjects">Optional objects to pass to string.Format.</param>
        public void WriteLine(string format, params object[] insertionObjects)
        {
            Write(format + Environment.NewLine, insertionObjects);
        }

        /// <summary>
        /// Disconnects the client from the remote console server and releases all resources held by the client.
        /// </summary>
        public void Close()
        {
            try
            {
                client.Close();
                outgoing.Close();
                incoming.Close();
            }
            catch
            {
                if (!InvisibleMode)
                    throw;
            }
        }

        /// <summary>
        /// Alias of Close().
        /// </summary>
        public void Dispose()
        {
            Close();
        }
    }
}

